﻿using System;

namespace Otus.Teaching.Generics.Customers.Core.Application.Exceptions
{
    public class EntityNotFoundException
        : Exception
    {
        
    }
}