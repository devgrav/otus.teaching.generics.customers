﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Generics.Customers.Core.Application.Services.Abstractions;
using Otus.Teaching.Generics.Customers.Core.Domain.Dto;
using Otus.Teaching.Generics.Customers.Core.Domain.Entities;
using Otus.Teaching.Generics.Customers.Core.Domain.Repositories.Abstractions;

namespace Otus.Teaching.Generics.Customers.Core.Application.Services
{
    public class OrderEntityService
        : IOrderService
    {
        private readonly IRepository<Order> _orderRepository;

        public OrderEntityService(IRepository<Order> orderRepository)
        {
            _orderRepository = orderRepository;
        }
        
        public async Task<List<OrderForListDto>> GetOrdersAsync()
        {
            var orders =  await _orderRepository.GetAllAsync();

            return orders.Select(x => new OrderForListDto()
            {
                Id = x.Id,
                Sum = x.Sum,
                CustomerId = x.CustomerId,
                FilialIdentifier = x.FilialIdentifier,
                PositionsCount = x.PositionsCount
            }).ToList();
        }
    }
}