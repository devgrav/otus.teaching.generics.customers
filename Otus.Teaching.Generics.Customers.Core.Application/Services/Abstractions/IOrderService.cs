﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Generics.Customers.Core.Domain.Dto;

namespace Otus.Teaching.Generics.Customers.Core.Application.Services.Abstractions
{
    public interface IOrderService
    {
        public Task<List<OrderForListDto>> GetOrdersAsync();
    }
}