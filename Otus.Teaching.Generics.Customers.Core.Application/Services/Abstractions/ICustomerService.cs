﻿﻿using System;
using System.Threading.Tasks;
 using Otus.Teaching.Generics.Customers.Core.Domain.Dto;

 namespace Otus.Teaching.Generics.Customers.Core.Application.Services.Abstractions
{
    public interface ICustomerService
    {
        Task<CustomersForListDto> GetCustomersForListAsync();
        
        Task<CustomersForListDto> GetFilteredCustomersForListAsync(CustomerFilterDto customerFilterDto);
        
        Task<CustomerDto> GetCustomerAsync(Guid id);
    }
}
