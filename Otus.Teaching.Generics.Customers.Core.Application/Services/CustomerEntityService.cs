﻿﻿﻿using System;
 using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
  using System.Transactions;
  using Otus.Teaching.Generics.Customers.Core.Application.Exceptions;
  using Otus.Teaching.Generics.Customers.Core.Application.Services.Abstractions;
  using Otus.Teaching.Generics.Customers.Core.Domain.Dto;
  using Otus.Teaching.Generics.Customers.Core.Domain.Repositories.Abstractions;
  using Otus.Teaching.Generics.Customers.Core.Domain.Entities;

  namespace Otus.Teaching.Generics.Customers.Core.Application.Services
{
    public class CustomerEntityService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerEntityService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }
        
        public async Task<CustomersForListDto> GetCustomersForListAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            return new CustomersForListDto()
            {
                Items = customers.Select(x => new CustomersForListItemDto()
                {
                    Id = x.Id,
                    Email = x.Email,
                    Channel = x.Channel.ToString(),
                    CreatedDate = x.CreatedDate.ToString("dd.MM.yyyy HH:mm:ss"),
                    FullName = x.FullName,
                    ActiveFilialId = x.ActiveFilialId
                }).ToList()
            };
        }

        public async Task<CustomersForListDto> GetFilteredCustomersForListAsync(CustomerFilterDto customerFilterDto)
        {
            var customers = await _customerRepository.GetAllByFilterAsync(customerFilterDto);

            return customers;
        }

        public async Task<CustomerDto> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if(customer == null)
                throw new EntityNotFoundException();
            
            return new CustomerDto()
            {
                Id = customer.Id,
                Channel = customer.Channel.ToString(),
                Email = customer.Email,
                CreatedDate = customer.CreatedDate.ToString("dd.MM.yyyy HH:mm:ss"),
                FullName = customer.FullName,
                IsActive = customer.IsActive,
                ActiveFilialId = customer.ActiveFilialId
            };
        }
    }
}
