﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Generics.Customers.Core.Application.Services.Abstractions;
using Otus.Teaching.Generics.Customers.Core.Domain.Entities;
using Otus.Teaching.Generics.Customers.Core.Domain.Repositories.Abstractions;
using Otus.Teaching.Generics.Customers.Infrastructure.DataAccess;
using Otus.Teaching.Generics.Customers.Infrastructure.DataAccess.DbInitialization;
using Otus.Teaching.Generics.Customers.Infrastructure.DataAccess.Repositories;

namespace Otus.Teaching.Generics.Customers.Host.WebApi.Composition
{
    public static class DalExtensions
    {
        public static IServiceCollection AddDalDependencies(this IServiceCollection services)
        {
            services.AddScoped<IDbInitializer, DropAndCreateDbInitializer>();
            
            services.AddDbContext<DataContext>(options =>
            {
                options.UseSqlite("Filename=OtusTeachingGenericsCustomersDb.sqlite");
            });
            
            services.AddScoped<IDbInitializer, DropAndCreateDbInitializer>();
            services
                .AddScoped<ICustomerRepository, EfCustomerRepository>()
                .AddScoped(typeof(IRepository<>), typeof(PermittedBaseRepository<>));

            return services;
        }
    }
}
