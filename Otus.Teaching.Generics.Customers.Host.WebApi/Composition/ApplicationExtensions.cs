﻿
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.Generics.Customers.Core.Application.Services;
using Otus.Teaching.Generics.Customers.Core.Application.Services.Abstractions;

namespace Otus.Teaching.Generics.Customers.Host.WebApi.Composition
{
    public static class ApplicationExtensions
    {
        public static IServiceCollection AddApplicationDependencies(this IServiceCollection services)
        {
             services
                .AddScoped<ICustomerService, CustomerEntityService>()
                .AddScoped<IOrderService, OrderEntityService>();;

             return services;
        }
    }
}
