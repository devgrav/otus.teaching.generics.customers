﻿﻿using System;
 using System.Net;
 using System.Threading.Tasks;
 using Microsoft.AspNetCore.Http;
 using Microsoft.AspNetCore.Mvc;
 using Microsoft.AspNetCore.Mvc.ModelBinding;
 using NSwag.Annotations;
 using Otus.Teaching.Generics.Customers.Core.Application.Services.Abstractions;
 using Otus.Teaching.Generics.Customers.Core.Domain.Dto;
 using Otus.Teaching.Generics.Customers.Core.Domain.Entities;

 namespace Otus.Teaching.Generics.Customers.Host.WebApi.Controllers
{
    /// <summary>
    /// API заказов
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrdersController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        /// <summary>
        /// Получить список заказов
        /// </summary>
        [HttpGet("list")]
        [ProducesResponseType(typeof(CustomersForListDto),StatusCodes.Status200OK)]
        public async Task<IActionResult> GetOrders()
        {
            return Ok(await _orderService.GetOrdersAsync());
        }
    }
}
