﻿﻿using System;
 using System.Net;
 using System.Threading.Tasks;
 using Microsoft.AspNetCore.Http;
 using Microsoft.AspNetCore.Mvc;
 using Microsoft.AspNetCore.Mvc.ModelBinding;
 using NSwag.Annotations;
 using Otus.Teaching.Generics.Customers.Core.Application.Exceptions;
 using Otus.Teaching.Generics.Customers.Core.Application.Services.Abstractions;
 using Otus.Teaching.Generics.Customers.Core.Domain.Dto;
 using Otus.Teaching.Generics.Customers.Core.Domain.Entities;

 namespace Otus.Teaching.Generics.Customers.Host.WebApi.Controllers
{
    /// <summary>
    /// API клиентов
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        [HttpGet("list")]
        [ProducesResponseType(typeof(CustomersForListDto),StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCustomers()
        {
            return Ok(await _customerService.GetCustomersForListAsync());
        }
        
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        [HttpGet("list/filtered")]
        [ProducesResponseType(typeof(CustomersForListDto),StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCustomers([FromQuery]CustomerFilterDto filter)
        {
            return Ok(await _customerService.GetFilteredCustomersForListAsync(filter));
        }
        
        /// <summary>
        /// Получить клиента по Id
        /// </summary>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(CustomerDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetCustomer(Guid id)
        {
            try
            {
                var customer = await _customerService.GetCustomerAsync(id);
                    
                return Ok(customer);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }
    }
}
