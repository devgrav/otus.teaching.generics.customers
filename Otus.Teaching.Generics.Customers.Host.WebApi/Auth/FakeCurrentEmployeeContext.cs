﻿using System;
using Otus.Teaching.Generics.Customers.Core.Domain;
using Otus.Teaching.Generics.Customers.Core.Domain.Entities;

namespace Otus.Teaching.Generics.Customers.Host.WebApi.Auth
{
    public class FakeCurrentEmployeeContext
        : ICurrentEmployeeContext
    {
        public Employee CurrentEmployee => new Employee()
        {
            Id = Guid.NewGuid(),
            FilialId = 1
        };
    }
}