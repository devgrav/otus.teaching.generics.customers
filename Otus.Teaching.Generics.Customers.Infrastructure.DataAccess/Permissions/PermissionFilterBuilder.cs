﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Otus.Teaching.Generics.Customers.Core.Domain;
using Otus.Teaching.Generics.Customers.Core.Domain.Entities;

namespace Otus.Teaching.Generics.Customers.Infrastructure.DataAccess.Permissions
{
    public class PermissionFilterBuilder<T>
        where T: DomainEntity
    {
        private readonly ICurrentEmployeeContext _currentEmployeeContext;

        public PermissionFilterBuilder(ICurrentEmployeeContext currentEmployeeContext)
        {
            _currentEmployeeContext = currentEmployeeContext;
        }
        
        public Expression<Func<T, bool>> Build()
        {
            if (typeof(T) == typeof(Customer))
            {
                Expression<Func<Customer, bool>> filter = x =>
                    x.ActiveFilialId == _currentEmployeeContext.CurrentEmployee.FilialId;

                return filter as Expression<Func<T, bool>>;
            }
            
            if (typeof(T) == typeof(Order))
            {
                Expression<Func<Order, bool>> filter = x =>
                    x.FilialIdentifier == _currentEmployeeContext.CurrentEmployee.FilialId;

                return filter as Expression<Func<T, bool>>;
            }

            throw new Exception("Не удалось найти подходящий тип для фильтрации по филиалам");
        }
    }
}