﻿﻿﻿namespace Otus.Teaching.Generics.Customers.Infrastructure.DataAccess.DbInitialization
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
