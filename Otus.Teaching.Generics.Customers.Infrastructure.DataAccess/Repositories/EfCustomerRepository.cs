﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Generics.Customers.Core.Domain;
using Otus.Teaching.Generics.Customers.Core.Domain.Dto;
using Otus.Teaching.Generics.Customers.Core.Domain.Entities;
using Otus.Teaching.Generics.Customers.Core.Domain.Repositories.Abstractions;

namespace Otus.Teaching.Generics.Customers.Infrastructure.DataAccess.Repositories
{
    public class EfCustomerRepository
        : PermittedBaseRepository<Customer>, ICustomerRepository
    {

        public EfCustomerRepository(DataContext dbContext, ICurrentEmployeeContext currentEmployeeContext)
            : base(dbContext, currentEmployeeContext)
        {
        }

        public async Task<CustomersForListDto> GetAllByFilterAsync(CustomerFilterDto filterDto)
        {
            var query = EntitySet.AsQueryable();

            if (!string.IsNullOrWhiteSpace(filterDto.FullName))
                query = query.Where(x => x.FullName.Contains(filterDto.FullName));
            if (filterDto.Channel.HasValue)
                query = query.Where(x => x.Channel == filterDto.Channel.Value);
            if (filterDto.CreatedDate.HasValue)
                query = query.Where(x => x.CreatedDate.Date == filterDto.CreatedDate.Value.Date);
            if (filterDto.CreatedDateLessThenOrEqual.HasValue)
                query = query.Where(x => x.CreatedDate <= filterDto.CreatedDateLessThenOrEqual.Value);
            if (filterDto.CreatedDateMoreThenOrEqual.HasValue)
                query = query.Where(x => x.CreatedDate >= filterDto.CreatedDateMoreThenOrEqual.Value);

            var orderedQuery = query.OrderBy(x => x.FullName);

            var projected =  orderedQuery
                .Select(x => new CustomersForListItemDto()
                {
                    Id = x.Id,
                    Channel = x.Channel.ToString(),
                    Email = x.Email,
                    CreatedDate = x.CreatedDate.ToString("dd.MM.yyyy HH:mm:ss"),
                    FullName = x.FullName,
                    IsActive = x.IsActive,
                    ActiveFilialId = x.ActiveFilialId
                });

            var items = await projected.ToListAsync();
            
            return new CustomersForListDto()
            {
                Items = items
            };
        }
    }
}
