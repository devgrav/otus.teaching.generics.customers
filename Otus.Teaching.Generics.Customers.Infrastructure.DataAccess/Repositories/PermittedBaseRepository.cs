﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Generics.Customers.Core.Domain;
using Otus.Teaching.Generics.Customers.Core.Domain.Entities;
using Otus.Teaching.Generics.Customers.Infrastructure.DataAccess.Permissions;

namespace Otus.Teaching.Generics.Customers.Infrastructure.DataAccess.Repositories
{
    public class PermittedBaseRepository<T>
        : BaseRepository<T>
        where T : DomainEntity
    {
        public PermittedBaseRepository(DataContext dbContext, ICurrentEmployeeContext currentEmployeeContext) 
            : base(dbContext)
        {
            var permissionFilterBuilder = new PermissionFilterBuilder<T>( currentEmployeeContext);
            var filter = permissionFilterBuilder.Build();
            EntitySet = EntitySet.Where(filter);
        }
    }
}