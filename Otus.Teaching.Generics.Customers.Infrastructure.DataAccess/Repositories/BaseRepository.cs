﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Generics.Customers.Core.Domain.Entities;
using Otus.Teaching.Generics.Customers.Core.Domain.Repositories.Abstractions;

namespace Otus.Teaching.Generics.Customers.Infrastructure.DataAccess.Repositories
{
    public class BaseRepository<T>
        : IRepository<T>
        where T: DomainEntity
    {
        protected IQueryable<T> EntitySet { get; set; }

        public BaseRepository(DataContext dbContext)
        {
            EntitySet = dbContext.Set<T>();
        }
        
        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await EntitySet.ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await EntitySet.SingleOrDefaultAsync(x => x.Id == id);
        }
    }
}