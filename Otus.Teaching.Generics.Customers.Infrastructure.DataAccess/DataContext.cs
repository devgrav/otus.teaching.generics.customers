﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Generics.Customers.Core.Domain.Entities;
using Otus.Teaching.Generics.Customers.Infrastructure.DataAccess.Mapping;

namespace Otus.Teaching.Generics.Customers.Infrastructure.DataAccess
{
    public class DataContext
        : DbContext
    {
        /// <summary>
        /// Customers
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Orders
        /// </summary>
        public DbSet<Order> Orders { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new CustomerMappingConfiguration());
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }
    }
}