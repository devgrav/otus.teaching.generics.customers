﻿using Otus.Teaching.Generics.Customers.Core.Domain.Entities;

namespace Otus.Teaching.Generics.Customers.Core.Domain
{
    public interface ICurrentEmployeeContext
    {
        public Employee CurrentEmployee { get; }
    }
}