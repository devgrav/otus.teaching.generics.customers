﻿using System;

namespace Otus.Teaching.Generics.Customers.Core.Domain.Entities
{
    public class Employee
    {
        public Guid Id { get; set; }

        public int FilialId { get; set; }
    }
}