﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.Generics.Customers.Core.Domain.Entities
{
	/// <summary>
	/// Customer
	/// </summary>
	public class Customer
		: DomainEntity
	{

	/// <summary>
	/// Полное имя клиента
	/// </summary>
	public string FullName { get; set; }

	/// <summary>
	/// Канал привлечения (интернет-реклама, реклама на улице и т.д.)
	/// </summary>
	public AcquisitionChannel Channel { get; set; }

	/// <summary>
	/// Дата создания
	/// </summary>
	public DateTime CreatedDate { get; set; }

	/// <summary>
	/// Email
	/// </summary>
	public string Email { get; set; }

	/// <summary>
	/// Страна
	/// </summary>
	public string Country { get; set; }

	/// <summary>
	/// Признак активности
	/// </summary>
	public bool IsActive { get; set; }

	/// <summary>
	/// Id филиала
	/// </summary>
	public int ActiveFilialId { get; set; }
	
	}
}

