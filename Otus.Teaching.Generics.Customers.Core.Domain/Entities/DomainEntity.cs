﻿using System;

namespace Otus.Teaching.Generics.Customers.Core.Domain.Entities
{
    public class DomainEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }
    }
}