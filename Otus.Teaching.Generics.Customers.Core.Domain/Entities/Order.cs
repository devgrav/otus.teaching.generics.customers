﻿using System;

namespace Otus.Teaching.Generics.Customers.Core.Domain.Entities
{
    public class Order
        : DomainEntity
    {
        public Guid CustomerId { get; set; }
        
        public decimal Sum { get; set; }

        public int PositionsCount { get; set; }

        public int FilialIdentifier { get; set; }
    }
}