﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Generics.Customers.Core.Domain.Dto;
using Otus.Teaching.Generics.Customers.Core.Domain.Entities;

namespace Otus.Teaching.Generics.Customers.Core.Domain.Repositories.Abstractions
{
    public interface ICustomerRepository
        : IRepository<Customer>
    {
        Task<CustomersForListDto> GetAllByFilterAsync(CustomerFilterDto filterDto);
    }
}