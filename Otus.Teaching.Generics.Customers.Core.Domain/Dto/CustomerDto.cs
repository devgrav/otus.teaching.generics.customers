﻿﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.Generics.Customers.Core.Domain.Dto
{
    public class CustomerDto
    {
        public Guid Id { get; set; }
        
        public string FullName { get; set; }
        
        public string Email { get; set; }
        
        public bool IsActive { get; set; }

        public string Channel { get; set; }

        public string CreatedDate { get; set; }

        public int ActiveFilialId { get; set; }
    }
}

