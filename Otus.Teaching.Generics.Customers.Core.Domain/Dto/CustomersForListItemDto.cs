﻿﻿ using System;

  namespace Otus.Teaching.Generics.Customers.Core.Domain.Dto
{
    public class CustomersForListItemDto
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        
        public string Email { get; set; }

        public string Channel { get; set; }

        public string CreatedDate { get; set; }

        public bool IsActive { get; set; }

        public int ActiveFilialId { get; set; }

    }
}
