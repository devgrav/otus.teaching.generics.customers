﻿using System.Collections.Generic;

namespace Otus.Teaching.Generics.Customers.Core.Domain.Dto
{
    public class CustomersForListDto
    {
        public List<CustomersForListItemDto> Items { get; set; }
    }
}
