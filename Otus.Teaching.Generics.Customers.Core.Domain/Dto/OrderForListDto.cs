﻿using System;
using Otus.Teaching.Generics.Customers.Core.Domain.Entities;

namespace Otus.Teaching.Generics.Customers.Core.Domain.Dto
{
    public class OrderForListDto
    {
        public Guid Id { get; set; }

        public Guid CustomerId { get; set; }
        
        public decimal Sum { get; set; }

        public int PositionsCount { get; set; }

        public int FilialIdentifier { get; set; }
    }
}